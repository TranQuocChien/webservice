﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew :IEmployee
    {
        EmployeeDataDataContext data = new EmployeeDataDataContext();


        public List<Employee_1> GetProductList()
        {
            try
            {
                return (from employee in data.Employee_1s select employee).ToList();
            }
            catch
            {
                return null;
            }
        }


        public bool AddEmployee(Employee_1 eml)
        {
            try
            {
                data.Employee_1s.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteEmployee(int idE)
        {
            try
            {
                Employee_1 employeeToDelete =
                    (from employee in data.Employee_1s where employee.employeeID == idE select employee).Single();
                data.Employee_1s.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
       
        public bool UpdateEmployee(Employee_1 eml)
        {
            Employee_1 employeeToModify =
                (from employee in data.Employee_1s where employee.employeeID == eml.employeeID select employee).Single();
            employeeToModify.Age = eml.Age;
            employeeToModify.address = eml.address;
            employeeToModify.firstName = eml.firstName;
            employeeToModify.lastName = eml.lastName;
            data.SubmitChanges();
            return true;
        }
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        private class employee
        {
        }
    }
}
